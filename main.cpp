#include <iostream>
using namespace std;

class Date
{
private:
    int day, month, year;

public:
    Date(); //без параметров

    Date(int day, int month, int year)
    {
        this->day = day;
        this->month = month;
        this->year = year;
    }; //  с параметрами

    Date(const Date& date)
    {
        setDate(date.day, date.month, date.year);
    } //  копирование

    ~Date()
    {
    } // Деструктор

    // Методы 
    void setDate(int, int, int);
    bool isValidDate(int, int, int);
    void displayDate();
    };

bool Date::isValidDate(int day, int month, int year)
{
    if (day < 1 || day > 31)
    {
        return false;
    }
    if (month < 1 || month > 12)
    {
        return false;
    }
    if (year < 1 || year > 2020)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void Date::displayDate()
{
    string monthArray[] = { "January", "February", "March","April", "May", "June", "July","August", "September", "October","November", "December" };

    if (isValidDate(day, month, year))
    {
        cout << day << " " << monthArray[month - 1] << " " << year << endl;
    }
    else
    {
        cout << "Error: incorrect date" << endl;
    }
}

void Date::setDate(int day, int month, int year)
{
    day = this->day;
    month = this->month;
    year = this->year;
}

int main()
{
    int day, month, year;

    cout << "Enter day (from 1 to 31)" << endl;
    cin >> day;

    cout << "Enter month (from 1 to 12)" << endl;
    cin >> month;

    cout << "Enter year (from 1 to 2020)" << endl;
    cin >> year;

    Date newDate(day, month, year);
    newDate.setDate(day, month, year);
    newDate.displayDate();

    return 0;
}